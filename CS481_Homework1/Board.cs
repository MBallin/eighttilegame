﻿/*Authors: Mark Ballin, Pablo Pelayo, Pitambar Paudel, Sandeep Tatikonda, Varun Rai 
 Class Name:State.cs
 This class takes the two dimensional array and has methods to  create states, generate next states,
 compare states if they are same, print states content.*/
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace CS481_Homework1
{
    public class Board
    {
        /// <summary>
        /// The width of the board
        /// </summary>
        private const int width = 3;

        /// <summary>
        /// The height of the board
        /// </summary>
        private const int height = 3;

        /// <summary>
        /// Has the object been disposed?
        /// </summary>
        private bool IsDisposed;

        /// <summary>
        /// Used to get a thread-safe reference for the object
        /// </summary>
        public SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        /// <summary>
        /// The current board state
        /// </summary>
        public int[,] state { get; set; } = new int[width,height];

        /// <summary>
        /// Default constructor which builds the initial state and goal state from a file named "in.txt"
        /// </summary>
        public Board()
        {
        }

        /// <summary>
        /// Constructor from int[,]
        /// </summary>
        /// <param name="newInitialState"></param>
        /// <param name="newGoalState"></param>
        public Board(int[,] oldState)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    this.state[i, j] = oldState[i, j];
                }
            }
        }

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="newInitialState"></param>
		/// <param name="newGoalState"></param>
		public Board(Board oldState)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    this.state[i, j] = oldState.state[i, j];
                }
            }
        }

		/// <summary>
		/// This function will generate a list of all possible new states based on a current state
		/// </summary>
		/// <param name="currentState"></param>
		/// <returns>List of new states</returns>
		public List<Board> generateNextsStates()
        {
            List<Board> newStates = new List<Board>();
            Board tempState = new Board(this);

            //Top left corner
            if(this.state[0,0] == -1)
            {
                tempState.state[0, 0] = state[0, 1];
                tempState.state[0, 1] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[0, 0] = state[1, 0];
                tempState.state[1, 0] = -1;
                newStates.Add(tempState);
            }

            // Top right corner
            else if (this.state[2, 0] == -1)
            {
                tempState.state[2, 0] = state[1, 0];
                tempState.state[1, 0] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[2, 0] = state[2, 1];
                tempState.state[2, 1] = -1;
                newStates.Add(tempState);
            }


            // Bottom left corner
            else if (this.state[0, 2] == -1)
            {
                tempState.state[0, 2] = state[0, 1];
                tempState.state[0, 1] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[0, 2] = state[1, 2];
                tempState.state[1, 2] = -1;
                newStates.Add(tempState);
            }

            // Bottom right Corner
            else if (this.state[2, 2] == -1)
            {
                tempState.state[2, 2] = state[1, 2];
                tempState.state[1, 2] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[2, 2] = state[2, 1];
                tempState.state[2, 1] = -1;
                newStates.Add(tempState);
            }

            // Center
            else if (this.state[1, 1] == -1)
            {
                tempState.state[1, 1] = state[0, 1];
                tempState.state[0, 1] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[1, 1] = state[1, 0];
                tempState.state[1, 0] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[1, 1] = state[1, 2];
                tempState.state[1, 2] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[1, 1] = state[2, 1];
                tempState.state[2, 1] = -1;
                newStates.Add(tempState);
            }

            // Top middle
            else if (this.state[1, 0] == -1)
            {
                tempState.state[1, 0] = state[0, 0];
                tempState.state[0, 0] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[1, 0] = state[2, 0];
                tempState.state[2, 0] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[1, 0] = state[1, 1];
                tempState.state[1, 1] = -1;
                newStates.Add(tempState);
            }

            // Left middle
            else if (this.state[0, 1] == -1)
            {
                tempState.state[0, 1] = state[0, 0];
                tempState.state[0, 0] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[0, 1] = state[0, 2];
                tempState.state[0, 2] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[0, 1] = state[1, 1];
                tempState.state[1, 1] = -1;
                newStates.Add(tempState);
            }

            // Right middle
            else if (this.state[2, 1] == -1)
            {
                tempState.state[2, 1] = state[2, 0];
                tempState.state[2, 0] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[2, 1] = state[2, 2];
                tempState.state[2, 2] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[2, 1] = state[1, 1];
                tempState.state[1, 1] = -1;
                newStates.Add(tempState);
            }

            // Bottom middle
            else if (this.state[1, 2] == -1)
            {
                tempState.state[1, 2] = state[0, 2];
                tempState.state[0, 2] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[1, 2] = state[2, 2];
                tempState.state[2, 2] = -1;
                newStates.Add(tempState);
                tempState = new Board(this);
                tempState.state[1, 2] = state[1, 1];
                tempState.state[1, 1] = -1;
                newStates.Add(tempState);
            }
            return newStates;
        }

		/// <summary>
		/// Overlaoded equals operator
		/// </summary>
		/// <param name="obj"></param>
		/// <returns>True if both states are the same.</returns>
		public override bool Equals(Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to State return false.
            Board p = obj as Board;
            if ((Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return this == p;
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(Board a, Board b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (a.state[i, j] != b.state[i, j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(Board a, Board b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Helper function for overloaded equals operator
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            int total = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    total ^= this.state[i, j];
                }
            }
            return total;
        }

        /// <summary>
        /// Prints to the console, any given state
        /// </summary>
        /// <param name="currentState"></param>
        public String toString()
        {
            String output = String.Empty;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (this.state[i, j] == -1)
                    {
                        output += "x ";
                    }
                    else
                    {
                        output += this.state[i, j] + " ";
                    }
                }
            }
            return output;
        }

        /// <summary>
        /// Prints out any specific row of a state
        /// </summary>
        /// <param name="row">The row to print</param>
        public String toString(int row)
        {
            String output = String.Empty;
            for (int i = 0; i < width; i++)
            {
                if (this.state[i, row] == -1)
                {
                    output += "x ";
                }
                else
                {
                    output += this.state[i, row] + " ";
                }
            }
            output += "   ";
            return output;
        }
        
        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

		/// <summary>
        /// Helper function for garbage collection.
        /// </summary>
        /// <param name="disposing"></param>
		protected virtual void Dispose(bool disposing)
        {
            if (IsDisposed)
                return;

            if (disposing)
            {
                handle.Dispose();
            }

            // Free any unmanaged objects here.
            //
            IsDisposed = true;
        }
    }
}
