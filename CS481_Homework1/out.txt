Algorithm: Hill Climbing
Heuristic: CountOutOfPlace
No solution found.

Algorithm: Hill Climbing
Heuristic: DistanceOutOfPlace
No solution found.

Algorithm: Hill CLimbing
Heuristic: Custom
No solution found.

Algorithm: A*
Heuristic: CountOutOfPlace
NumOfMoves: 14
2 x 5    2 3 5    2 3 5    2 3 5    2 3 5    
1 3 8    1 x 8    1 8 x    1 8 4    1 8 4    
7 6 4    7 6 4    7 6 4    7 6 x    7 x 6    

2 3 5    2 3 5    2 3 x    2 x 3    x 2 3    
1 x 4    1 4 x    1 4 5    1 4 5    1 4 5    
7 8 6    7 8 6    7 8 6    7 8 6    7 8 6    

1 2 3    1 2 3    1 2 3    1 2 3    
x 4 5    4 x 5    4 5 x    4 5 6    
7 8 6    7 8 6    7 8 6    7 8 x    

Algorithm: A*
Heuristic: DistanceOutOfPlace
NumOfMoves: 16
2 x 5    2 3 5    2 3 5    2 3 5    2 3 5    
1 3 8    1 x 8    1 6 8    1 6 8    1 6 x    
7 6 4    7 6 4    7 x 4    7 4 x    7 4 8    

2 3 5    2 3 5    2 3 5    2 3 5    2 3 x    
1 x 6    1 4 6    1 4 6    1 4 x    1 4 5    
7 4 8    7 x 8    7 8 x    7 8 6    7 8 6    

2 x 3    x 2 3    1 2 3    1 2 3    1 2 3    
1 4 5    1 4 5    x 4 5    4 x 5    4 5 x    
7 8 6    7 8 6    7 8 6    7 8 6    7 8 6    

1 2 3    
4 5 6    
7 8 x    

Algorithm: A*
Heuristic: Custom
NumOfMoves: 14
2 x 5    2 3 5    2 3 5    2 3 5    2 3 5    
1 3 8    1 x 8    1 8 x    1 8 4    1 8 4    
7 6 4    7 6 4    7 6 4    7 6 x    7 x 6    

2 3 5    2 3 5    2 3 x    2 x 3    x 2 3    
1 x 4    1 4 x    1 4 5    1 4 5    1 4 5    
7 8 6    7 8 6    7 8 6    7 8 6    7 8 6    

1 2 3    1 2 3    1 2 3    1 2 3    
x 4 5    4 x 5    4 5 x    4 5 6    
7 8 6    7 8 6    7 8 6    7 8 x    

