﻿/*
 * Authors: Mark Ballin, Pablo Pelayo, Pitambar Paudel, Sandeep Tatikonda, Varun Rai 
 */
using System;
using System.Collections.Generic;
using System.IO;

namespace CS481_Homework1
{
    class hw1
    {
        static void Main(string[] args)
        {
            //Declare variables.
            String inputFile = "in.txt";
            String outputFile = "out.txt";
            Board initialState = new Board();
            Board goalState = new Board();

            //Build initial and goal state from file
            try
            {
                String path = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, @"..\..\"));
                using (StreamReader sr = File.OpenText(path + inputFile))
                {
                    string[] board = new String[3];
                    for (int i = 0; i < 3; i++)
                    {
                        board[i] = sr.ReadLine();
                    }
                    initialState = buildState(board);
                    sr.ReadLine();
                    for (int i = 0; i < 3; i++)
                    {
                        board[i] = sr.ReadLine();
                    }
                    goalState = buildState(board);
                }

                using (StreamWriter sr = new StreamWriter(path + outputFile))
                {
                    //Board startingBoard = GenerateGameBoard(goalState, 25);

                    sr.WriteLine("Algorithm: Hill Climbing");
                    sr.WriteLine("Heuristic: CountOutOfPlace");
                    sr.WriteLine(hillClimbing(initialState, goalState, HeuristicType.CountOutOfPlace));

                    sr.WriteLine("Algorithm: Hill Climbing");
                    sr.WriteLine("Heuristic: DistanceOutOfPlace");
                    sr.WriteLine(hillClimbing(initialState, goalState, HeuristicType.DistanceOutOfPlace));

                    sr.WriteLine("Algorithm: Hill CLimbing");
                    sr.WriteLine("Heuristic: Custom");
                    sr.WriteLine(hillClimbing(initialState, goalState, HeuristicType.Hybrid));

                    sr.WriteLine("Algorithm: A*");
                    sr.WriteLine("Heuristic: CountOutOfPlace");
                    sr.WriteLine(A_Star(initialState, goalState, HeuristicType.CountOutOfPlace));

                    sr.WriteLine("Algorithm: A*");
                    sr.WriteLine("Heuristic: DistanceOutOfPlace");
                    sr.WriteLine(A_Star(initialState, goalState, HeuristicType.DistanceOutOfPlace));

                    sr.WriteLine("Algorithm: A*");
                    sr.WriteLine("Heuristic: Custom");
                    sr.WriteLine(A_Star(initialState, goalState, HeuristicType.Hybrid));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

		/// <summary>
		/// This function implemets a steepest-ascent/descent algorithm to solve the eight tile game
		/// </summary>
		/// <param name="tree"></param>
		/// <returns>True: found a solution within 100 moves.  False otherwise.</returns>
		public static String hillClimbing(Board initialState, Board goalBoard, HeuristicType heuristic)
        {
            //Declare and initalize variables
            Tree tree = new Tree(initialState, goalBoard, heuristic);
            Node currentNode = tree.root;
            currentNode.DistanceFromHome = 0;

            while (currentNode.currentBoard != goalBoard && tree.bestPath.Count < 100 && currentNode.DistanceFromHome < 10000)
            {
                //Expand node
                tree.createChildren(currentNode);

                tree.bestPath = buildPath(tree.root, currentNode);


                foreach (Node n in currentNode.children)
                {
                    // Compute g(n)
                    n.DistanceFromHome = currentNode.DistanceFromHome + 1;
                    
                    // f(n) += g(n)
                    n.HeuristicScore += n.DistanceFromHome;

                    // Add only unique states
                    if (!tree.openQueue.Contains(n))
                    {
                        tree.openQueue.Enqueue(n);
                    }
                }

                // Move to best node of the children
                // If there are multiple best children, randomly pick one to move to
                List<Node> tempBestNodes = new List<Node>();
                tree.bestScore = tree.openQueue.Peek().HeuristicScore;
                
                // Add all scores which match the current best score to the list
                while (!tree.openQueue.IsEmpty && tree.openQueue.Peek().HeuristicScore == tree.bestScore)
                {
                    tempBestNodes.Add(tree.openQueue.Dequeue());
                }

                // If there are multiple choices, pick one randomly and move to it.
                if (tempBestNodes.Count > 1)
                {
                    currentNode = tempBestNodes[new Random().Next(0, tempBestNodes.Count)];
                }

                // Otherwise, move to the first best solution
                else
                {
                    currentNode = tempBestNodes[0];
                }

                //Remove other children
                while (!tree.openQueue.IsEmpty)
                {
                    tree.openQueue.Dequeue();
                }
            }

            if (tree.bestPath.Count >= 100)
            {
                return "No solution found." + Environment.NewLine;
            }
            else
            {
                return "NumOfMoves: " + tree.bestPath.Count + Environment.NewLine + PrintPath(tree.bestPath, 5);
            }
        }

        /// <summary>
        /// Implementaion of A* algorithm
        /// </summary>
        /// <param name="initialState"></param>
        /// <param name="goalBoard"></param>
        /// <param name="heuristic"></param>
        /// <returns>True: found a solution within 100 moves.  False otherwise.</returns>
        public static String A_Star(Board initialState, Board goalBoard, HeuristicType heuristic)
		{
            //Declare and initalize variables
			int numOfMoves = 0;
			Tree tree = new Tree(initialState, goalBoard, heuristic);
            tree.root.DistanceFromHome = 0;
			Node currentNode = tree.root;
            tree.openQueue.Enqueue(tree.root);

            while (currentNode.currentBoard != goalBoard && currentNode.DistanceFromHome <= 100 && !tree.openQueue.IsEmpty)
			{
                //tree.bestPath = buildPath(tree.root, currentNode);
                numOfMoves = tree.bestPath.Count;

                //Expand node
                tree.createChildren(currentNode);
                tree.closedQueue.Enqueue(currentNode);
                foreach (Node n in currentNode.children)
				{
                    n.DistanceFromHome = currentNode.DistanceFromHome + 1;
                    n.HeuristicScore += n.DistanceFromHome;
                    if (!tree.closedQueue.Contains(n))
                    {
                        if (!tree.openQueue.Contains(n))
                        {
                            tree.openQueue.Enqueue(n);
                        }
                    }
				}

                if(!tree.openQueue.IsEmpty)
                {              
                    //Move to best node of the children
                    currentNode = tree.openQueue.Dequeue();
                }

            }

            if(numOfMoves > 100)
            {
                return "No Solution found" + Environment.NewLine;
            }
            else
            {
                tree.bestPath = buildPath(tree.root, currentNode);
                return "NumOfMoves: " + tree.bestPath.Count + Environment.NewLine + PrintPath(tree.bestPath, 5);
            }
        }
        
         /// <summary>
        /// Implementaion of Breadth First Search algorithm
        /// </summary>
        /// <param name="initialState"></param>
        /// <param name="goalState"></param>
        /// <returns>True: found a solution within 100 moves.  False otherwise.</returns>
        static public void bfsSearch(Board initialState, Board goalState)
        {
            Tree tree = new Tree(initialState, goalState, HeuristicType.Count); // just added heuristic type randomly to provide an argument
            Node currentNode = tree.root;
            currentNode.HeuristicScore = 0;
            tree.openQueue.Enqueue(currentNode);

            // while the open queue isn't empty
            while (!tree.openQueue.IsEmpty && currentNode.HeuristicScore < 15)
            {
                // use level as the heuristic score for sorting
                currentNode = tree.openQueue.Dequeue();

                if (currentNode.currentBoard == goalState)
                {
                    Console.WriteLine("Solution found");
                    tree.bestPath = buildPath(tree.root, currentNode);
                    Console.WriteLine("NumOfMoves: " + tree.bestPath.Count);
                    PrintPath(tree.bestPath, 10);
                    return;
                } 
                else
                {
                    // generate children for current node and move current node to the closed queue
                    // move children to the open queue
                    tree.createChildren(currentNode);
                    if (!tree.closedQueue.Contains(currentNode))
                    {
                        tree.closedQueue.Enqueue(currentNode);
                    }

                    foreach (Node n in currentNode.children)
                    {
                        n.HeuristicScore = currentNode.HeuristicScore + 1;
                        if (!tree.openQueue.Contains(n))
                        {
                            tree.openQueue.Enqueue(n);
                        }
                    }
                }
            }
            Console.WriteLine("Unable to find a solution");
        }

        /// <summary>
        /// Prints the path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="numOfMoves">The number of moves.</param>
        public static String PrintPath(List<Node> path, int rowLength)
        {
            String output = String.Empty;
            while(path.Count >= rowLength)
            {
                output += PrintRows(path, rowLength);
                path.RemoveRange(0, rowLength);
                output += Environment.NewLine;
            }
            output += PrintRows(path, path.Count);
            return output;
        }

        /// <summary>
        /// Prints the rows.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="rowLength">Length of the row.</param>
        public static String PrintRows(List<Node> path, int rowLength)
        {
            String output = String.Empty;
            for (int i = 0; i < rowLength; i++)
            {
                output += path[i].currentBoard.toString(0);
            }
            output += Environment.NewLine;
            for (int i = 0; i < rowLength; i++)
            {
                output += path[i].currentBoard.toString(1);
            }
            output += Environment.NewLine;
            for (int i = 0; i < rowLength; i++)
            {
                output += path[i].currentBoard.toString(2);
            }
            output += Environment.NewLine;
            return output;
        }

        /// <summary>
        /// Builds the path.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="goal">The goal.</param>
        /// <returns></returns>
        public static List<Node> buildPath(Node start, Node goal)
        {
            List<Node> path = new List<Node>();
            Node current = goal;
            while(current != start)
            {
                path.Add(current);
                current = current.parent;
            }
            path.Add(start);
            path.Reverse();
            return path;
        }

        /// <summary>
        /// This method builds a state object from an array.
		/// </summary>
		/// <param name="line"></param>
		/// <returns>A state object.</returns>
		public static Board buildState(string[] line)
        {
            int[,] tempState = new int[3, 3];
            for (int i = 0; i < 3; i++)
            {
                string[] tiles = line[i].Split(' ');
                for (int j = 0; j < 3; j++)
                {
                    if (tiles[j].Equals("x"))
                    {
                        tempState[j, i] = -1;
                    }
                    else
                    {
                        tempState[j, i] = Int32.Parse(tiles[j]);
                    }
                }
            }
            return new Board(tempState);
        }

        /// <summary>
        /// Generates the game board.
        /// </summary>
        /// <param name="goalState">State of the goal.</param>
        /// <param name="numOfMoves">The number of moves.</param>
        /// <returns></returns>
        public static Board GenerateGameBoard(Board goalState, int numOfMoves)
        {
            Board initialState = goalState;
            List<Board> temp;
            List<Board> pastStates = new List<Board>();
            pastStates.Add(goalState);

            while(numOfMoves > 0)
            {
                temp = initialState.generateNextsStates();
                initialState = new Board(temp[new Random().Next(0, temp.Count)]);
                if(!pastStates.Contains(initialState))
                {
                    numOfMoves--;
                    pastStates.Add(new Board(initialState));
                }
            }
            return initialState;
        }
    }
}
