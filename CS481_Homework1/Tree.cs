﻿/*
 * Authors: Mark Ballin, Pablo Pelayo, Pitambar Paudel, Sandeep Tatikonda, Varun Rai 
 * 
 */
using System.Collections.Generic;

namespace CS481_Homework1
{
    class Tree
    {
        /// <summary>
        /// Gets or sets the number of nodes.
        /// </summary>
        /// <value>
        /// The number of nodes.
        /// </value>
        public int numOfNodes { get; set; } = 0;

        /// <summary>
        /// Gets or sets the best score.
        /// </summary>
        /// <value>
        /// The best score.
        /// </value>
        public double bestScore { get; set; } = 99999;

        /// <summary>
        /// Gets or sets the root.
        /// </summary>
        /// <value>
        /// The root.
        /// </value>
        public Node root { get; set; } = null;

        /// <summary>
        /// Queue for the nodes which have yet to be visited
        /// </summary>
        public PriorityQueue<Node> openQueue;

        /// <summary>
        /// The visited nodes
        /// </summary>
        public Queue<Node> closedQueue;

        /// <summary>
        /// The path we are currently on
        /// </summary>
        public List<Node> bestPath;

        /// <summary>
        /// Heuristic Type
        /// </summary>
        public HeuristicType heuristic { get; set; } = HeuristicType.CountOutOfPlace;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tree"/> class.
        /// </summary>
        /// <param name="startingBoard">The starting board.</param>
        /// <param name="numberOfMoves">The number of moves.</param>
        public Tree(Board startingBoard, Board goalBoard, HeuristicType type)
        {
            this.root = new Node(startingBoard, goalBoard, type);
            this.openQueue = new PriorityQueue<Node>();
            this.closedQueue = new Queue<Node>();
            this.bestPath = new List<Node>();
            this.bestPath.Add(this.root);
            this.heuristic = type;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Tree"/> class.
        /// </summary>
        public Tree()
        {
            this.root = new Node();
            this.openQueue = new PriorityQueue<Node>();
            this.closedQueue = new Queue<Node>();
            this.bestPath = new List<Node>();
            this.heuristic = HeuristicType.CountOutOfPlace;
        }

        /// <summary>
        /// Expands out a node.
        /// </summary>
        /// <param name="parent"></param>
        /// <returns>A list of the nodes newly created children.</returns>
        public List<Node> createChildren(Node parent)
        {
            foreach (Board s in parent.currentBoard.generateNextsStates())
            {
                Node newNode = new Node(s, parent.goalBoard, this.heuristic);
                newNode.parent = parent;
                parent.children.Add(newNode);
            }
            return parent.children;
        }

        /// <summary>
        /// Deletes the tree.
        /// </summary>
        /// <param name="root">The root.</param>
        /// <returns></returns>
        private void deleteTree(Node root)
        {
            if (root.children.Count == 0)
            {
                root.Dispose();
            }
            else
            {
                foreach (Node child in root.children)
                {
                    deleteTree(child);
                }
            }
        }
    }
}
