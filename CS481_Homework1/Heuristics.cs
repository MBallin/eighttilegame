/*Authors: Mark Ballin, Pablo Pelayo, Pitambar Paudel, Sandeep Tatikonda, Varun Rai
 Class Name: Heuristics
 This Class is static and defines three different static methods to calculate heuritics of the  
states under consideration*/

using System;
using System.Collections.Generic;
using System.Drawing;

namespace CS481_Homework1
{
    /// <summary>
    /// Enumeration of heuristic types
    /// </summary>
    public enum HeuristicType : int
    {
        CountOutOfPlace = 0,
        DistanceOutOfPlace = 1,
        Hybrid = 2,
        Count = 3
    };

	static class Heuristics
    {
        public static int BoardWidth = 3;
        public static int BoardHeight = 3;

        /// <summary>
        /// Calculates heuristic of a state based on the tiles out of the place as compared with the goal state
        /// </summary>
        /// <param name="currentState"></param>
        /// <param name="goalState"></param>
        /// <returns>Total count of all tiles out of place</returns>
        public static int CountingTilesOutOfPlace(Board currentState, Board goalState)
        {
            int total = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (currentState.state[j, i] != goalState.state[j, i] && currentState.state[j,i] != -1)
                    {
                        total++;
                    }
                }
            }
            return total;
        }

        /// <summary>
        /// Calculates heuristic of a state based on the distance of the tiles from the position of the corresponding tile in the goal state
        /// </summary>
        /// <param name="currentState"></param>
        /// <param name="goalState"></param>
        /// <returns>Total distance of all tiles out of place.</returns>
		public static int DistanceTilesOutofPlace(Board currentState, Board goalState)
        {
            int total = 0;
            Point goalPosition = new Point();
            for (int i = 0; i < BoardWidth; i++)
            {
                for (int j = 0; j < BoardHeight; j++)
                {
                    if (currentState.state[i, j] != goalState.state[i, j] && currentState.state[i,j] != -1)
                    {
                        for (int k = 0; k < BoardWidth; k++)
                        {
                            for (int l = 0; l < BoardHeight; l++)
                            {
                                if (currentState.state[i, j] == goalState.state[k, l])
                                {
                                    goalPosition.X = k;
                                    goalPosition.Y = l;
                                }
                            }
                        }
                        total += Math.Abs((i - goalPosition.X) + (j - goalPosition.Y));
                    }
                }
            }
            return total;
        }

        /// <summary>
        /// Customs the hueristic.
        /// </summary>
        /// <param name="currentBoard">The current board.</param>
        /// <param name="goalBoard">The goal board.</param>
        /// <param name="positionMultiplier">The position multiplier.</param>
        /// <param name="distanceMultiplier">The distance multiplier.</param>
        /// <param name="reversalMultiplier">The reversal multiplier.</param>
        /// <returns></returns>
        public static int CustomHueristic(Board currentBoard, Board goalBoard, int positionMultiplier, int distanceMultiplier, int reversalMultiplier)
        {
            int total = 0;
            for (int i = 0; i < BoardWidth; i++)
            {
                for (int j = 0; j < BoardHeight; j++)
                {
                    if(!checkRowAndCol(goalBoard, currentBoard.state[j,i], j, i))
                    {
                        total++;
                    }
                }
            }
            return total;
        }

        /// <summary>
        /// Checks the row and col.
        /// </summary>
        /// <param name="currentBoard">The current board.</param>
        /// <param name="value">The value.</param>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns></returns>
        private static bool checkRowAndCol(Board currentBoard, int value, int x, int y)
        {
            for (int i = 0; i < BoardWidth; i++)
            {
                for (int j = 0; j < BoardHeight; j++)
                {
                    if(x == j || y == i)
                    {
                        if(currentBoard.state[j,i] == value)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}
 