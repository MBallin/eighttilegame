﻿/*
 * Authors: Mark Ballin, Pablo Pelayo, Pitambar Paudel, Sandeep Tatikonda, Varun Rai 
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CS481_Homework1
{
    class PriorityQueue<P> : IEnumerable
    {
        /// <summary>
        /// Underlying data structure.
        /// </summary>
        private List<P> list = new List<P>();

        /// <summary>
        /// Adds a value to the list then sorts the list using IComparable objects.
        /// </summary>
        /// <param name="item"></param>
        public void Enqueue(P item)
        {
            list.Add(item);
            list.Sort();
        }

        /// <summary>
        /// Removes an element from the front of the list.
        /// </summary>
        /// <returns>The first object from the List if it exists. Otherwise it returns a default object.</returns>
        public P Dequeue()
        {
            P temp;
            if(!IsEmpty)
            {
                temp = list.First();
                list.RemoveAt(0);
                return temp;
            }
            return default(P);
        }

        /// <summary>
        /// Counts this instance.
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            return list.Count;
        }

        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(P item)
        {
            foreach (P i in list)
            {
                if (item.Equals(i))
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Returns a copy fo the first element in the list without removing it.
        /// </summary>
        /// <returns>a copy fo the first element in the list</returns>
        public P Peek()
        {
            if(!IsEmpty)
            {
                return list.First();
            }
            return default(P);
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)list).GetEnumerator();
        }

        /// <summary>
        /// Is the list empty?
        /// </summary>
        public bool IsEmpty
        {
            get { return !list.Any(); }
        }
    }
}
