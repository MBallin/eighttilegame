﻿/* 
 * Authors: Mark Ballin, Pablo Pelayo, Pitambar Paudel, Sandeep Tatikonda, Varun Rai
 * This class takes the two dimensional array and has methods to  create states, generate next states,
 * compare states if they are same, print states content.
 */
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace CS481_Homework1
{
    public class Node : IDisposable, IComparable<Node>
    {
        /// <summary>
        /// Custom field for any extra information about the node
        /// </summary>
        public string label { get; set; } = string.Empty;

        /// <summary>
        /// The nodes parent
        /// </summary>
        public Node parent { get; set; } = null;

        /// <summary>
        /// A list of all the nodes children
        /// </summary>
        public List<Node> children { get; set; } = null;

        /// <summary>
        /// The current state of the node
        /// </summary>
        public Board currentBoard { get; set; } = null;

        /// <summary>
        /// The goal state.
        /// </summary>
        public Board goalBoard { get; set; } = null;

        /// <summary>
        /// The current nodes score.
        /// </summary>
        public int HeuristicScore { get; set; } = -1;

        /// <summary>
        /// The current nodes score.
        /// </summary>
        public int DistanceFromHome { get; set; } = -1;

        /// <summary>
        /// Has the node been visited already?
        /// </summary>
        public bool isVisited { get; set; } = false;

        /// <summary>
        /// Is the node disposed?
        /// </summary>
        public bool IsDisposed = false;

        /// <summary>
        /// Provides a thread-safe handle to the object.
        /// </summary>
        public SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Node(){}

        /// <summary>
        /// Secondary constructor
        /// </summary>
        /// <param name="newNode"></param>
        /// <param name="goalNode"></param>
        /// <param name="heuristic"></param>
        public Node(Board newNode, Board goalNode, HeuristicType heuristic)
        {
            this.currentBoard = newNode;
            this.goalBoard = goalNode;
            switch (heuristic)
            {
                case HeuristicType.CountOutOfPlace:
                    this.HeuristicScore = Heuristics.CountingTilesOutOfPlace(this.currentBoard, this.goalBoard);
                    break;
                case HeuristicType.DistanceOutOfPlace:
                    this.HeuristicScore = Heuristics.DistanceTilesOutofPlace(this.currentBoard, this.goalBoard);
                    break;
                case HeuristicType.Hybrid:
                    this.HeuristicScore = Heuristics.CustomHueristic(this.currentBoard, this.goalBoard, 2 , 1 , 1);
                    break;
            }
            this.children = new List<Node>();
            this.parent = new Node();
        }

        /// <summary>
        /// Tertiary constructor
        /// </summary>
        /// <param name="newNode"></param>
        public Node(Node newNode)
        {
            this.currentBoard = newNode.currentBoard;
            this.goalBoard = newNode.goalBoard;
            this.children = newNode.children;
            this.parent = newNode.parent;
        }

        /// <summary>
        /// Helper function for garbage collection.
        /// </summary>
        /// <param name="disposing"></param>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

		/// <summary>
        /// Helper function for garbage collection.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (IsDisposed)
                return;

            if (disposing)
            {
                this.handle.Dispose();
                this.currentBoard.Dispose();
            }

            this.handle.Dispose();
            this.currentBoard.Dispose();
            IsDisposed = true;
        }

        /// <summary>
        /// Overlaoded equals operator
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>True if both states are the same.</returns>
        public override bool Equals(Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Node return false.
            Node p = obj as Node;
            if ((Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return this == p;
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(Node a, Node b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            return a.currentBoard == b.currentBoard;
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(Node a, Node b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Helper function for overloaded equals operator
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.currentBoard.GetHashCode();
        }
        /// <summary>
        /// Used to implement IComparable<Node> interface
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        int IComparable<Node>.CompareTo(Node other)
        {
            // If parameter is null return false.
            if (other == null)
            {
                return -1;
            }

            // Return true if the fields match:
            if (this.HeuristicScore < other.HeuristicScore)
            {
                return -1;
            }
            if (this.HeuristicScore > other.HeuristicScore)
            {
                return 1;
            }
            return 0;
        }
    }
}
